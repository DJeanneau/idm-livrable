Contenu du livrable:
1) Eclipse Luna avec plugins intégrés pour l'édition de plans de vol;
2) Un workspace Eclipse contenant des fichiers d'exemple (à ouvrir avec l'Eclipse fourni);
3) Une archive contenant les plugins du projet qui peuvent être ajoutés à une autre versions d'Eclipse pour utiliser l'éditeur (EMF, Sirius et Xtext sont nécessaires);
4) Le document de STBE du DSL;
5) Un dossier src contenant:
	- La grammaire Xtext;
	- Le fichier .ecore du métamodèle généré;
	- Une image .png représentant le diagramme du métamodèle;
	- Le fichier .odesign de description de l'interface Sirius.

Extrait de l'annexe du document:

PRISE EN MAIN DES ÉDITEURS GRAPHIQUE ET TEXTUEL

1) Copier dans le répertoire de votre choix l'archive Linux ou Windows fournie selon
votre système d'exploitation, puis clic droit->extraire ici.

2) Une fois l'extraction terminée, double cliquez sur le répertoire eclipse, puis sur l'icône
Eclipse.

3) Dans Eclipse, pour créer un nouveau fichier :
 - dans l'éditeur graphique :
	clic droit->New->Other->Flightplan->Flightplan Model
 - dans l'éditeur textuel :
	clic droit->New->File, puis donner au fichier un nom en .flightplan

4) Ajouter le Viewpoint Flightplan :
	clic droit sur le projet ->Viewpoint selection

5) Ouvrir des fichiers dans :
 - l'éditeur graphique, déplier le fichier, clic droit sur le Model->new Representation
 - l'éditeur textuel, double clic sur le fichier
